use clap::Parser;
use std::collections::HashMap;
use std::fs::read;
use std::path::PathBuf;

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();
    println!("{:?}", cli);

    let greetings = greetings_table();
    println!("{:#?}", greetings);
    println!("Ivan: {:?} {}", greetings.get("Ivan"), greetings.contains_key("Ivan"));

    match who(&cli)? {
        Name::Default => {
            eprintln!("booring!");
        }
        Name::Given(name) => {
            println!("name: {:?}", name);
            if greetings.contains_key(&name) {
                let greet = greetings.get(&name).unwrap();
                println!("{}, {}", greet, name);
            } else {
                println!("Hello, {}", name);
            }
        }
    }
    Ok(())
}

#[derive(Debug, Parser)]
struct Cli {
    #[clap(short, long, default_value = "world")]
    name: String,

    #[clap(short, long)]
    filename: Option<PathBuf>,
}

enum Name {
    Default,
    Given(String),
}

fn who(cli: &Cli) -> anyhow::Result<Name> {
    if let Some(filename) = &cli.filename {
        let name = read(filename)?;
        let name = String::from_utf8(name)?;
        Ok(Name::Given(name.trim().to_string()))
    } else if cli.name == "world" {
        Ok(Name::Default)
    } else {
        Ok(Name::Given(cli.name.clone()))
    }
}

fn greetings_table() -> HashMap<String, String> {
    let mut table = HashMap::new();
    table.insert("Ivan".to_string(), "moi".to_string());
    table.insert("Lars".to_string(), "terve".to_string());
    table
}
